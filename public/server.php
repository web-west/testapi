<?php
    require __DIR__.'/../vendor/autoload.php';
    require __DIR__.'/../app/config.php';

    use ShopExpress\ApiClient\ApiClient;
    use ShopExpress\ApiClient\Response\ApiResponse;

    $ApiClient = new ApiClient(
        $config['API_KEY'],
        $config['API_USER'],
        $config['API_URL']
    );

    $orders = $ApiClient->get("orders", [ 'start' => $_GET['start'] , 'limit' => $_GET['length'] ]);
    $data = [
        "recordsTotal" => $orders->total,
        "data" => []
    ];

    

    foreach($orders->orders as $key => $order) {
        $data['data'][] = [
            "order_id" => $order['order_id'],
            'summ' => $order['summ']
        ];
    }

    echo json_encode($data);
?>