<!DOCTYPE html>
<html lang="xxs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</head>
<body>

    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Order ID</th>
                <th>SUMM</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Order ID</th>
                <th>SUMM</th>
            </tr>
        </tfoot>
    </table>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        ;(function ($) {
            $(document).ready(function () {
                $('#example').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ordering": false,
                    "info":     false,
                    "ajax": "./server.php",
                    "columns": [
                        { "data": "order_id" },
                        { "data": "summ" }
                    ]
                } );
            })
        })(jQuery)
    </script>

</body>
</html>